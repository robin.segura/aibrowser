// objects.js
var express = require('express');
var router = express.Router();

let objects = [
  {
    id: 1,
    name: 'object 1',
    address: 'url.string.fr',
    subobjects: [
      { id: 1, name: 'Subobject 1' },
      { id: 2, name: 'Subobject 2' },
    ],
  },
  {
    id: 2,
    name: 'object 2',
    address: 'String.fr',
    subobjects: [
      { id: 3, name: 'Subobject 3' },
      { id: 4, name: 'Subobject 4' },
    ],
  },
];


let model = {
  description: 'Structure of objects and subobjects',
  object: {
    id: 'Number',
    name: 'String',
    address: 'String',
    subobjects: [
      {
          id: 'Number',
          name: 'String',
          type: 'subobject'
      }
    ]
  }
};

router.get('/', function(req, res, next) {
  res.json(objects);
});

router.get('/model', function(req, res, next) {
    res.json(model);
});

module.exports = router;