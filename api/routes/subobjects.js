// subobjects.js
var express = require('express');
var router = express.Router();

let subobjects = [
  {
    id: 1,
    name: 'subObj 1',
    date: new Date(),
    objectId: 1
  },
  {
    id: 2,
    name: 'subObj 2',
    date: new Date(2024, 5, 25),
    objectId: 1
  },
  {
    id: 3,
    name: 'subObj 3',
    date: new Date(2024, 5, 31),
    objectId: 2
  },
  {
    id: 4,
    name: 'subObj 4',
    date: new Date(2024, 6, 5),
    objectId: 2
  },
];

let model = {
  description: 'Structure of subobjects and parent objects',
  subobject: {
    id: 'Number',
    name: 'String',
    date: 'Date',
    object: {
      id: 'Number',
      name: 'String',
      type: 'object'
    },
  }
};

router.get('/:pid', function (req, res, next) {
  const pid = parseInt(req.params.pid, 10);
  const csubobject = subobjects.find(subobject => subobject.objectId === pid);

  if (csubobject) {
    res.json(csubobject);
  } else {
    res.status(404).json(subobjects, { message: 'Subobject not found'});
  }
});

router.get('/model', function (req, res, next) {
  res.json(model);
});

module.exports = router;