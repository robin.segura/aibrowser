// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [
    "@pinia/nuxt", 
    "nuxt-primevue",
    // 'nuxt-proxy'
  ],
  primevue: {
    options: {
      cssLayerOrder: 'reset,primevue,primeicons',
    },
  },
  // routeRules: {
  //   "/api/**": { proxy: import.meta.env.API_URL },
  // },
  css: ['primevue/resources/themes/mdc-dark-indigo/theme.css']
})