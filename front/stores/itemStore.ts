// stores/itemStore.ts
import { defineStore, acceptHMRUpdate } from "pinia"
import axios from 'axios'

interface Item {
  id: string;
  name: string;
  content: {};
  subitems?: Subitem[];
}

interface Subitem {
  id: string;
  name: string;
  item?: Item;
}

export const useItemStore = defineStore('itemStore', {
  state: () => ({
    items: [] as Item[],
    subitems: [] as Subitem[],
    endpoints: [] as string[],
    currentType: '' as string,
    currentSubType: '' as string
  }),
  actions: {
    async fetchEndpoints() {
      try {
        const response = await axios.get<string[]>('/api/endpoints')
        this.endpoints = response.data
      } catch (error) {
        console.error('Failed to fetch endpoints:', error)
      }
    },
    async fetch(itemType: string) {
      try {
        const response = await axios.get<Item[]>(`/api/${itemType}`)
        this.items = response.data
      } catch (error) {
        console.error('Failed to fetch items:', error)
      }
    },
    async fetchSubitems(subItemType: string, itemId: string) {
      try {
        const response = await axios.get<Subitem[]>(`/api/${subItemType}`, {
          params: {
            pid: `${itemId}` // Replace 'param' with the actual parameter name and 'value' with the desired value
          }
        })
        this.subitems = response.data
      } catch (error) {
        console.error('Failed to fetch subitems:', error)
      }
    }
  }
})
if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useItemStore, import.meta.hot))
}
